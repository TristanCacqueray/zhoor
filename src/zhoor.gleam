import argv
import gleam/bit_array as bita
import gleam/bytes_builder
import gleam/erlang/process.{type Subject}
import gleam/http.{Post}
import gleam/http/request.{type Request}
import gleam/http/response.{type Response}
import gleam/io
import gleam/json.{type Json, array, object, string, to_string}
import gleam/list
import gleam/option.{type Option, None, Some}
import gleam/otp/actor
import gleam/result
import gleam/string
import mist.{type Connection, type ResponseData}

pub fn main() {
  case argv.load().arguments {
    ["cache"] -> start_cache()
    ["relay"] -> start_relay()
    _ -> {
      io.println("Not implemented command")
      Nil
    }
  }
}

fn start_relay() {
  todo
}

fn incoming_request_to_json(ir: IncomingRequest) -> Json {
  let request_to_json = fn(r: Request(BitArray)) -> Json {
    let headers =
      r.headers
      |> list.fold([], fn(acc, kv) {
        let #(k, v) = kv
        acc |> list.append([#(k, string(v))])
      })
    let body = case r.body |> bita.to_string {
      Ok(body) -> body
      Error(_) -> panic
    }
    object([
      #("host", string(r.host)),
      #("headers", object(headers)),
      #("body", string(body)),
    ])
  }
  object([
    #("conn_name", string(ir.conn_name)),
    #("req", ir.req |> request_to_json),
  ])
}

fn start_cache() {
  let assert Ok(cache) = actor.start([], cache_handle)
  let assert Ok(antispoof) = actor.start(cache, antispoof_handle)
  let assert Ok(detector) = actor.start(antispoof, detector_handle)

  // For the WebSocket
  let selector = process.new_selector()
  let ws_client_state = cache

  let handle_payload = fn(request: Request(Connection), conn_name: String) -> Response(
    ResponseData,
  ) {
    mist.read_body(request, 1024 * 1024 * 10)
    // Content length or 10 MBytes max
    |> result.map(fn(req) {
      case req.method {
        Post -> {
          process.send(detector, DetectorPush(IncomingRequest(conn_name, req)))
          response.new(200)
          |> response.set_body(mist.Bytes(bytes_builder.new()))
        }
        _ -> {
          io.println("Unable to handle non Post request")
          response.new(400)
          |> response.set_body(mist.Bytes(bytes_builder.new()))
        }
      }
    })
    |> result.lazy_unwrap(fn() {
      io.println("Unable to handle the request")
      response.new(400)
      |> response.set_body(mist.Bytes(bytes_builder.new()))
    })
  }

  let not_found =
    response.new(404)
    |> response.set_body(mist.Bytes(bytes_builder.new()))

  let assert Ok(_) =
    fn(req: Request(Connection)) -> Response(ResponseData) {
      case request.path_segments(req) {
        ["connection", conn_name, "payload"] -> handle_payload(req, conn_name)
        ["ws"] ->
          mist.websocket(
            request: req,
            on_init: fn(conn) {
              io.println("Hello client !")
              io.debug(conn)
              #(ws_client_state, Some(selector))
            },
            on_close: fn(_state) { io.println("goodbye!") },
            handler: handle_ws_message,
          )
        _ -> not_found
      }
    }
    |> mist.new
    |> mist.port(3000)
    |> mist.start_http

  process.sleep_forever()
}

fn handle_ws_message(state, conn, message) {
  case message {
    mist.Text("ping") -> {
      let assert Ok(_) = mist.send_text_frame(conn, "pong")
      actor.continue(state)
    }
    mist.Text("give me all") -> {
      let assert Ok(data) = process.call(state, CacheTake, 10)
      let serialized_data =
        data |> array(incoming_request_to_json) |> to_string |> bita.from_string
      let assert Ok(_) = mist.send_binary_frame(conn, serialized_data)
      actor.continue(state)
    }
    // TODO Use mist.Custom instead ?
    mist.Text(_) | mist.Binary(_) | mist.Custom(_) -> {
      actor.continue(state)
    }
    mist.Closed | mist.Shutdown -> actor.Stop(process.Normal)
  }
}

type IncomingRequest {
  IncomingRequest(conn_name: String, req: Request(BitArray))
}

type ActorMessage(e) {
  Shutdown
  DetectorPush(push: IncomingRequest)
  AntiSpoofPush(push: ProviderRequest)
  CachePush(push: IncomingRequest)
  CacheTake(reply_with: Subject(Result(List(IncomingRequest), Nil)))
}

type ProviderRequest {
  GitHubRequest(req: IncomingRequest)
  GitLabRequest(req: IncomingRequest)
}

// This actor detects the payload provider type
// https://docs.github.com/en/webhooks/webhook-events-and-payloads#delivery-headers
fn detector_handle(
  message: ActorMessage(e),
  state: Subject(ActorMessage(e)),
) -> actor.Next(ActorMessage(e), Subject(ActorMessage(e))) {
  let provider_type = fn(i_req: IncomingRequest) -> Option(ProviderRequest) {
    let user_agent =
      i_req.req
      |> request.get_header("User-Agent")
      |> result.unwrap("Unknown-Agent")
    case user_agent {
      "Unknown-agent" -> None
      _ ->
        case user_agent |> string.split("/") {
          ["GitLab", _] -> Some(GitLabRequest(i_req))
          ["GitHub-Hookshot", _] -> Some(GitHubRequest(i_req))
          _ -> None
        }
    }
  }
  case message {
    Shutdown -> actor.Stop(process.Normal)
    DetectorPush(data) -> {
      io.println("Detector received data")
      case data |> provider_type() {
        Some(pr) -> {
          process.send(state, AntiSpoofPush(pr))
          actor.continue(state)
        }
        None -> actor.continue(state)
      }
    }
    _ -> actor.continue(state)
  }
}

fn antispoof_handle(
  message: ActorMessage(e),
  state: Subject(ActorMessage(e)),
) -> actor.Next(ActorMessage(e), Subject(ActorMessage(e))) {
  let validate_authenticity = fn(r: ProviderRequest) -> Option(IncomingRequest) {
    case r {
      // TODO: Implement the validation
      GitLabRequest(ir) -> Some(ir)
      // TODO: Implement the validation
      GitHubRequest(ir) -> Some(ir)
    }
  }
  case message {
    Shutdown -> actor.Stop(process.Normal)
    AntiSpoofPush(data) ->
      case data |> validate_authenticity() {
        Some(valid) -> {
          process.send(state, CachePush(valid))
          actor.continue(state)
        }
        None -> actor.continue(state)
      }
    _ -> actor.continue(state)
  }
}

fn cache_handle(
  message: ActorMessage(e),
  state: List(IncomingRequest),
) -> actor.Next(ActorMessage(e), List(IncomingRequest)) {
  case message {
    Shutdown -> actor.Stop(process.Normal)
    CachePush(data) -> {
      let new_state = list.append(state, [data])
      actor.continue(new_state)
    }
    CacheTake(client) -> {
      case state |> list.split(10) {
        #(taken, rest) -> {
          process.send(client, Ok(taken))
          actor.continue(rest)
        }
      }
    }
    _ -> actor.continue(state)
  }
}
