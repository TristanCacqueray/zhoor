# zhoor

[![Package Version](https://img.shields.io/hexpm/v/zhoor)](https://hex.pm/packages/zhoor)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/zhoor/)

```sh
gleam add zhoor
```
```gleam
import zhoor

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/zhoor>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```
